# DatePoll-Frontend

[![pipeline status](https://gitlab.com/BuergerkorpsEggenburg/datepoll-frontend/badges/development/pipeline.svg)](https://gitlab.com/DatePoll/datepoll-frontend/commits/development)

## Installation

- Clone the repo
- Install nodejs and npm
- Install Angular CLI
- Have fun

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` in the `src`-folder to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
