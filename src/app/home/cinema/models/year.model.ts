export class Year {
  public id: number;
  public year: string;

  constructor(id: number, year: string) {
    this.id = id;
    this.year = year;
  }
}
